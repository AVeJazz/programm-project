import java.util.*;
public class SupplyList {
    // Сущность – Поставочный лист
    // поля скрыты в классе
    private final static String SUPPLY_FORMAT_STRING =
            "Поставка: %-s, %-5d "; //формат записи о поставке
    private String name; //название группы
    private List <Batch> batch; // список (набор) партий
    // 1)конструкторы
    public SupplyList() {
        name= ""; //без названия
        batch = new ArrayList <Batch>(); //создается пустой список
    }
    public SupplyList(String name){
        this.name=name; // с названием поставки
        batch = new ArrayList <Batch>(); //создается пустой список
    }
    public SupplyList(String name, List list){
        this.name=name; // с названием поставки
        batch = new ArrayList <Batch>(list); //создается на основе
        // существующего списка
    }
    // 2)геттеры для всех полей
    public String getSupplyName(){return name;}
    public List <Batch> getBatch(){return batch;}
    // 3)геттер, возвращающий партию по id
    public Batch getBatch(int id){
        for (Batch bat:batch)
            if (bat.getId() == id) return bat;
        return null;
    }
    // 4)геттер, возвращающий число партий деталей
    public int getBatchNumber(){
        return batch.size();
    }
    // 5)сеттер для поля шифр поставки
    public void setSupplyName(String name){this.name = name;}
    //Запросы на вставку, удаление, изменение данных:
    // 6) Добавить партию в список с учётом id
    public boolean addBatch(Batch bat){
        //партию нельзя добавить, если id занят
        if (getBatch(bat.getId())!=null)
            return false;
        if (batch.add(bat))
            return true;
        else return false;
    }
    // 7) удаление партиии по id
    public boolean delBatch(int id){
        //Удалить партии из поставки
        if (batch.remove(new Batch(id,"","",0.0)))
            return true;
        else return false;
    }
    // 8) возвращение средней цены
    public double avgPrice(){
        int num=batch.size(); if (num==0) return 0; double avg=0;
        for (Batch bat:batch) avg=avg + bat.getPrice();
        return avg/num;
    }


    //9) Вернуть партии, цена на которые ниже среднего
    public SupplyList aboveAvgPrice(){
        double avg=avgPrice();
        SupplyList supply = new SupplyList (name +
                ": партии, у которых цена ниже среднего - " + avg);
        //для просмотра (перечисления) объектов списка
        //используем цикл for-each
        for (Batch bat:batch)
            if (bat.getPrice()<avg)supply.addBatch(bat); return supply;
    }

    // 10) возвращение списка партий, цена детали в которых находится в заданном интервале
    public SupplyList betweenPrice(float p1, float p2){
        //Вернуть партии с ценой в диапазоне [p1,p2]
        SupplyList supply = new SupplyList(String.format (
                "%s: партии, у которых цена деталей в диапазоне от %4.2f до %4.2f", name,p1,p2));
        //для просмотра (перечисления) объектов списка
        //используем итератор
        Iterator <Batch> iter = batch.iterator();
        while (iter.hasNext()){
            Batch bat = iter.next();
            if ((bat.getPrice() >= p1)&&(bat.getPrice() <= p2))
                supply.addBatch(bat);
        }
        return supply;
    }
    // 11.1) возвращение отсортированного в естественном порядке
    public SupplyList sort(){
        //Cортировка в естественном порядке
        //Естественный порядок задает метод CompareTo,
        //переопределенный  в классе Batch
        SupplyList supply = new SupplyList (name, batch);
        Collections.sort(supply.batch);
        return supply;
    }
    // 11.2) возвращение отсортированного по условию
    public SupplyList sort(Comparator comp){
        //coртировка партий
        //по правилу, задаваемому компаратором comp
        SupplyList supply = new SupplyList (name, batch);
        Collections.sort(supply.batch, comp);
        return supply;
    }
    // 12) Вывод списка фирм в окно терминала
    public void putSupplyList(){
        System.out.println(name); //название фирмы
        System.out.printf("%5s%15s%17s%19s%17s\n",
                "Номер","Номер детали","Название","Количество","Цена");
        //заголовки столбцов
        int i=1;
        for (Batch bat:batch){
            System.out.printf(" %-8d %-18s %-16s %-20s  %-5.1f\n", i, bat.getId(),
                    bat.getName(), bat.getCount(),bat.getPrice());
            i=i+1;
        }
    }
    // 13) toString()
    public String toString(){
        return String.format(SUPPLY_FORMAT_STRING,name,getBatchNumber());
    }

    //Доп.метод №1
    //обновление ID у объекта
    public boolean updateId(int id,int newId){
        //Проверка не занят ли newId
        for (Batch bat:batch)
            if (bat.getId() == newId)
                return false;
        getBatch(id).setId(newId);
        return true;
    }
    //Доп.метод №2
    //Удаление всех объектов, где цена < среднего
    //Используется метод removeIf из Java 8
    //Имеет вид Collection.removeIf(Predicate<? super E> filter)
    //Метод перебирает коллекцию, и удаляет те элементы,
    //которые соответствуют filter
    public void delAllBelowAverage(){
        //подсчёт среднего значения не в формуле, поскольку при каждом
        //проходе значени будет меняться
        double avg= avgPrice();
        batch.removeIf(batch -> batch.getPrice() < avg);
    }
    //Доп.метод №3
    //возвращает список, названия элементов которого начинаются с letters
    public SupplyList filterStringStartWith(String letters){
        SupplyList sup = new SupplyList();
        Iterator <Batch> supplyIterator = batch.iterator();
        while (supplyIterator.hasNext()){
            Batch bat = supplyIterator.next();
            if (bat.getName().startsWith(letters))
                sup.addBatch(bat);
        }
        return sup;
    }
}
