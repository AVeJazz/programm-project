public class SupplyListDemo{
    //Для удобства переноса строки написания сообщений
    //в окно терминала созданы два метода nl
    //Перенос строки new line
    public static void nl(){System.out.println();};
    //Перенос строки + вывод текста в скобках
    public static void nl(String text){System.out.println(text);};
    // Взаимодействие поставок и партий
    public static void main (String[ ] args){
        //Создаём партии

        SupplyList sup1=new SupplyList ("Видеокарты");
        SupplyList sup2=new SupplyList ("Процессоры");
        // в массив args при запуске метода main (String[ ] args)
        // вводим 10 поставок по 5 партий.

        int j=0; //индекс массива args
        // добавление партий в поставку
        for (int i=0; i<5; i++){
            int id=Integer.valueOf(args[j]); j++;  // получено int значение переменной id
            String name=args[j]; j++;
            String count=args[j]; j++;
            //получаем double-эквивалент из String
            double price=Double.valueOf(args[j]); j++;
            //получаем int-эквивалент из String
            sup1.addBatch(new Batch(id, name, count,price));
        }
        // добавление маршрутов во вторую поставку
        for (int i=0; i<5; i++){
            int id = Integer.valueOf(args[ j ]); j++;  // получено int значение переменной id
            String name=args[j]; j++;
            String count=args[j]; j++;
            //получаем double-эквивалент из String
            double price=Double.valueOf(args[j]); j++;
            sup2.addBatch(new Batch(id, name, count, price));
        }
        //Выборка данных
        // Выводим всевозможные списки поставок
        // Выводим основной список без сортировки
        System.out.println("Поставки (без сортировки)");
        sup1.putSupplyList();
        System.out.println();
        // попытка добавить мпартию с уже занятым id
        System.out.println("Попытка добавить партии: (32323, NVidia, 500 ,50000.0)");
        if(!sup1.addBatch(new Batch (32323, "Nvidia", "500", 50000.0)))
            System.out.println("Не удалось добавить партию");
        System.out.println();
        // снова выводим основной список
        // (теперь с естественным порядком сортировки)
        System.out.println("Поставка (с естественным порядком сортировки)");
        sup1.sort().putSupplyList();
        System.out.println();
        // другие списки:
        System.out.println("Поставка (с сортировкой по возрастанию цены)");
        sup1.aboveAvgPrice().sort(new CompPriceAsc()).putSupplyList();
        System.out.println();
        System.out.println(
                "Поставка (с сортировкой по убыванию цены)");
        sup1.betweenPrice(4000.0f,7000.0f).sort(new CompPriceDesc()).putSupplyList();
        // удаление маршрута по id
        sup1.delBatch(32323);
        System.out.println();
        //выводим список поставок sup1 после удаления
        System.out.println("После удаления поставки c id=32323:");
        System.out.println("Поставка (с естественным порядком сортировки)");
        sup1.sort().putSupplyList();
        System.out.println();
        // Выводим список партий для второй поставки
        System.out.println(
                "Партия (с сортировкой по возрастанию имени и");
        System.out.println(" убыванию цены)");
        sup2.sort(new CompNameAscPriceDesc()).putSupplyList();
        System.out.println();
        // другие списки для второй поставки:
        System.out.println("Поставка 2 (с сортировкой по возрастанию цены)");
        sup2.aboveAvgPrice().sort(new CompPriceAsc()).putSupplyList();
        System.out.println();
        System.out.println("Поставка 2(с сортировкой по убыванию цены)");
        sup2.betweenPrice(10000.0f,20000.0f).sort(new CompPriceDesc()).putSupplyList();
        System.out.println();
        //Проверяем, есть ли в первой поставке партия
        // с заданным id
        int n=12345;
        Batch bat1=sup1.getBatch(n);
        if (bat1==null) System.out.printf (
                "В поставке %12s нет партии с id = %-3d %n", sup1.getSupplyName(),n);
        else System.out.println(bat1);
        n=1234;
        bat1=sup1.getBatch(n);
        if (bat1==null) System.out.printf (
                "В поставке %12s нет партии с id = %3s %n", sup1.getSupplyName(),n);
        else System.out.println(bat1);
        //Проверяем,есть ли во второй поставке
        //партия с id
        n=12345;
        bat1=sup2.getBatch(n);
        if (bat1==null) System.out.printf (
                "В поставке %12s нет партии с id = %-3d %n", sup2.getSupplyName(),n);
        else System.out.println(bat1);
    }
}

