
public class Batch implements Comparable <Batch> {
    // Сущность - Партия
    // поля скрыты в классе
    private final static String BATCH_FORMAT_STRING =
            "Партия: %-7d | %18s | %-8s | %-8.2f |";
    private int id; // id детали (ключевое)
    private String name; // название детали
    private String count; // количество деталей
    private double price; // цена детали
    // конструктор без параметров
    public Batch(){
        id = 0; name = ""; count = "";
        price = 0.0;
    }
    // конструктор с параметрами
    public Batch(int id, String name, String count,
                 double price){
        this.id=id;
        this.name=name;
        this.count=count;
        this.price=price;
    }
    //методы-геттеры
    public int getId(){return id;}
    public String getName(){return name;}
    public String getCount(){return count;}
    public double getPrice(){return price;}
    //методы-сеттеры
    public void setId(int id){this.id = id;}
    public void setName(String name){this.name = name;}
    public void setCount(String count){
        this.count = count;}
    public void setPrice(double length){this.price=price;}
    //Переопределяется метод toString класса Object
    //(возвращает строку описания объекта)
    @Override //проверяем переопределение
    public String toString(){
        return String.format(BATCH_FORMAT_STRING,id,name,
                count,price);
    }
    //Переопределяется метод equals класса Object
    //(задает способ сравнения объектов на равенство,
    //возвращает true, если запускающий объект
    //равен объекту-параметру)
    @Override //проверяем переопределение
    public boolean equals (Object ob){
        if (ob==this) return true; // ссылки равны – один
        // и тот же объект
        if (ob==null) return false; //если в метод передана null-ссылка
        //одинакового ли класса объекты
        if (getClass()!=ob.getClass())return false;
        Batch bat=(Batch)ob; // преобразование Object в Batch
        return id == bat.id; //id – ключевое поле объекта
    }
    //Переопределяется метод hashCode класса Object
    //Возвращает хэш-код объекта
    //(у равных объектов должны быть равные hash-коды)
    @Override //проверяем переопределение
    public int hashCode(){
        return 13* (new Integer(id)).hashCode();
    }
    //Определяем метод СоmpareTo интерфейса Сomparable
    //Для определения естественного порядка перечисления элементов
    public int compareTo(Batch bat){ if (id < bat.id) return -1;
    else if (id == bat.id) return 0; else return 1;
    }
}

